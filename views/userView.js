class UserView {
  showUserData(res, payload) {
    res.render("user", payload);
  }
}

module.exports = UserView;
