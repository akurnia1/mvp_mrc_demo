class User {
  constructor() {
    this.users = require("../db/users.json");
  }

  #getUsers() {
    return this.users;
  }

  getAllUser() {
    return this.#getUsers();
  }

  getUserById(id) {
    const users = this.#getUsers();

    let userFiltered = users.filter((user) => user.id == id);

    return userFiltered;
  }
}

module.exports = User;
