class Post {
  constructor() {
    this.posts = require("../db/posts.json");
  }

  #getPosts() {
    return this.posts;
  }

  getAllPosts() {
    return this.#getPosts();
  }

  getPostById(id) {
    const posts = this.#getPosts();

    let postFiltered = posts.filter((post) => post.id == id);

    return postFiltered;
  }
}

module.exports = Post;
