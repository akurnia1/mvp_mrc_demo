const Post = require("../models/Post");

const post = new Post();
class PostController {
  getAllPosts(req, res) {
    let posts = post.getAllPosts();
    res.json({
      message: "Get All Post",
      data: posts,
    });
  }

  getPostById(req, res) {
    let id = req.params.id;
    let postOne = post.getPostById(id);
    res.json({
      message: "get detail post by id",
      data: postOne,
    });
  }
}

module.exports = PostController;
