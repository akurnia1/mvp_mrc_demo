const express = require("express");
const cors = require("cors");
const app = express();

const routeUser = require("./routes/users");
const routePost = require("./routes/posts");

app.use(cors());

var bodyParser = require("body-parser");
app.set("view engine", "ejs");

// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// route for user

// route for posts

app.get("/", (req, res) => {
  res.json({
    message: "Server running",
  });
});

app.use("/users", routeUser);

app.use("/posts", routePost);

app.listen(3000, () => {
  console.log("listening on port 3000");
});
